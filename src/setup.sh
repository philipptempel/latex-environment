#!/usr/bin/env bash

mkdir ~/install \
    && cd ~/install \
    && git clone https://gitlab.com/philipptempel/latex-package.git \
    && cd latex-package \
    && make ins install \
    && cd ~ \
    && rm -rf ~/install \
