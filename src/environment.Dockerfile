ARG TEXLIVE_SCHEME
FROM registry.gitlab.com/philipptempel/docker-ubuntu-tug-texlive/2021:$TEXLIVE_SCHEME

MAINTAINER Philipp Tempel <docker@philipptempel.me>

ENV DEBIAN_FRONTEND noninteractive

RUN tlmgr update --self
RUN tlmgr install latexmk

WORKDIR /setup
RUN git clone https://gitlab.com/philipptempel/latex-package.git \
    && cd latex-package \
    && make ins install
RUN rm -rf /setup

CMD ["latexmk"]
