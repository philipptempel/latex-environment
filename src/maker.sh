#!/bin/sh

if [ $# -lt 2 ]; then
    >&2 echo "Illegal number of parameters"
    return 1
fi

DEBUG=""

ACTION="$1"; shift;
TEXLIVE_SCHEMES="$@"

CI_REGISTRY=${CI_REGISTRY:-"registry.gitlab.com"}
CI_PROJECT_PATH=${CI_PROJECT_PATH:-"philipptempel/latex-environment"}
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-"${CI_REGISTRY}/${CI_PROJECT_PATH}"}

BFLAGS=${BFLAGS:-""}


TAG_PREFIX="$CI_REGISTRY_IMAGE:"

PRG="$0"
while [ -h "$PRG" ] ; do
   PRG=`readlink "$PRG"`
done

SRCDIR=`dirname $PRG`

set -x

case $ACTION in
  build)
    for TEXLIVE_SCHEME in $TEXLIVE_SCHEMES; do
      # Build the main image
      $DEBUG docker build \
        --tag ${TAG_PREFIX}${TEXLIVE_SCHEME} \
        --file "$SRCDIR/environment.Dockerfile" \
        --build-arg TEXLIVE_SCHEME="${TEXLIVE_SCHEME}" \
        ${BFLAGS} \
        ${SRCDIR}

    done
    ;;
  push)
    for TEXLIVE_SCHEME in $TEXLIVE_SCHEMES; do
      # Push the main image
      $DEBUG docker push "${TAG_PREFIX}${TEXLIVE_SCHEME}"

    done
    ;;
esac

set +x
