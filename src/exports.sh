# LaTeX Docker image
export LATEX_IMAGE_TAG="full"
export LATEX_IMAGE_IMAGE="registry.gitlab.com/philipptempel/latex-environment"
export LATEX_IMAGE="$LATEX_IMAGE_IMAGE:$LATEX_IMAGE_TAG"
export LATEX_CONTAINER_NAME="LaTeX__$LATEX_IMAGE_TAG"
