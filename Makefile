SHELL=/bin/bash

TEXLIVE_YEAR ?= latest

SCHEMES = basic small medium full
BUILDS_FULL = $(patsubst %,build-%,$(SCHEMES))
PUSHES_FULL = $(patsubst %,push-%,$(SCHEMES))

# order of creation
# 1) basic
# 2) small
# 3) medium
# 4) full

# All target
.PHONY: all
all: build push

# Build everything
.PHONY: build
build: $(BUILDS_FULL)

# Push everything
.PHONY: push
push: $(PUSHES_FULL)

# Generic scheme target
build-container-%:
	./src/maker.sh build $*

# Generic push target
push-container-%:
	./src/maker.sh push $*

# Basic depends on minimal
build-basic: build-container-basic
push-basic: push-container-basic
basic: build-basic push-basic

# Small depends on basic
build-small: build-basic build-container-small
push-small: push-basic push-container-small
small: basic build-small push-small

# Medium depends on small
build-medium: build-small build-container-medium
push-medium: push-small push-container-medium
medium: small build-medium push-medium

# Full depends on medium
build-full: build-medium build-container-full
push-full: push-medium push-container-full
full: medium build-full push-full
